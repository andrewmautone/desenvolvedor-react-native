const datalake = require('../database/datalake')
const bcrypt = require('bcrypt');
const {
    validateUser,
    createError,
    validateEmail
} = require('../helper/helper')
const jwt = require('jsonwebtoken');
const jwtConfig = require('../jwtConfig/jwt.json')

class User {

    index = async(id) => {

        return await datalake.get("user/", {
            params: {
                id: id
            }

        }).then(user => {

            if (!user.data[0])
                createError("Usuario Inválido")

            return {...user.data[0],
                password: undefined
            }

        })




    }
    update = async(id, body) => {

        let validatedUser = validateUser(body)
        const hash = await bcrypt.hash(validatedUser.password, 12)
        validatedUser = {...validatedUser,
            password: hash
        }

        return await datalake.put("user/" + id, validatedUser).then(user => {


            if (!user.data)
                createError("Erro na atualzação dos dados")

            return {...user.data,
                password: undefined
            }


        })
    }
    login = async(body) => {

        if (!validateEmail(body.email))
            createError("Email inválido")

        if (!body.password)
            createError("Senha inválida")

        //verifica se o email existe e pega dados do usuario
        const user = await datalake.get("user/", {
            params: {
                email: body.email
            }

        }).then(user => {
            if (!user.data[0])
                createError("Email não cadastrado")

            return user.data[0]
        })


        const passwordValid = await bcrypt.compare(body.password, user.password)

        if (!passwordValid)
            createError("Senha incorreta")

        const token = jwt.sign({
                id: user.id
            },
            jwtConfig.secret,
            jwtConfig.expiresIn
        )

        return {user:{...user,password:undefined},token: token}


    }
    insert = async(body) => {


        let validatedUser = validateUser(body)

        //Encripta a senha
        const hash = await bcrypt.hash(validatedUser.password, 12)



        //Troca senha pelo hash
        validatedUser = {
            ...validatedUser,
            password: hash
        }

       
        const newUser = await datalake.post("user/", validatedUser).then(user => {

            if (!user.data)
                createError("Erro na criação do usuario")

            return user.data

        })

        //Gera o JWT
        const token = jwt.sign({
                id: newUser.id
            },
            jwtConfig.secret,
            jwtConfig.options
        )
        //Retorna infomações do usuario
        return {user:{...newUser,password:undefined},token: token}

    

    }
    verifyEmail = async(email) => {

        if (!validateEmail(email))
            createError("Email inválido")

        return await datalake.get("user/", {
            params: {
                email: email
            }

        }).then(user => {

            if (user.data[0])
                createError("Email já cadastrado")

            return true;

        })
    }





}

module.exports = new User;