const express = require('express')
const router = express.Router();
const productModel = require('../models/productModel')

router.get('/', async(req, res) => {
    try {
        
        const response = await productModel.all()
        return res.status(200).send(response)

    } catch (err) {
       
        return res.status(404).send(err)

    }

})
router.get('/:idProduto', async(req, res) => {
   
    try {

        const response = await productModel.index(req.params.idProduto)
        return res.status(200).send(response)

    } catch (err) {

        return res.status(404).send(err)

    }


})

module.exports = app => app.use('/api/v1/products', router)