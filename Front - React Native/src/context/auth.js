import React,{createContext,useState} from 'react';
import api from '../services/api';
import {createError,createSuccess} from '../helper/helpers'
import AsyncStorage from '@react-native-community/async-storage';
const AuthContext = createContext({});

function validateEmail(email) {
    const  re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

export const AuthProvider = ({children})=>{
    const [userInfo,setUserInfo] = useState(null);
    const [userToken,setUserToken] = useState(null);
    const [Loading,setLoading] = useState(false);
    
    //Pegar informações do usuario 
    async function  getInfoAsyncStorage(){
        
        setLoading(true)
        setUserInfo(JSON.parse(await AsyncStorage.getItem("@Eleven_userInfo")))
        setUserToken(JSON.parse(await AsyncStorage.getItem("@Eleven_token")))
        setLoading(false)

    }
    async function  getInfo(){
        
      return await api("GET", '/api/v1/user/' + userInfoContext.id)

    }
    //Sair da conta 
    async function logOut(){
      await AsyncStorage.setItem("@Eleven_token",JSON.stringify(null))
      setUserToken(null)
    }

   async function logIn(email, password){
        if(!validateEmail(email)){
        return createError("Email inválido");
        }
       
        if(password.length < 2){
        return createError("Senha inválida");
        }
        
        setLoading(true);
        var response;
        
        //Faz o post para autenticar
        try{
            
        response = await api("POST","/api/v1/user/auth",{email:email.toLowerCase(),password})
        }
        catch(err){
            
            setLoading(false);
            return createError("Erro no server");
        }

        if(response.status == "error"){
        setLoading(false);
        return createError(response.error?response.error:"Erro no servidor");
        }

        //Guarda os dados do usuario
        try{

          await AsyncStorage.multiSet([["@Eleven_userInfo",JSON.stringify(response.user)],["@Eleven_token",JSON.stringify(response.token)]])
        }
        catch(err){
         
          setLoading(false);
          return createError(err);
        }
        setUserInfo(response.user)
        setUserToken(response.token)
        setLoading(false);
        return createSuccess(response)
     


    }

   async function register(name,email, password,product_id){
   
        if(name.length < 3){
        return createError("Nome inválido");
        }
        
        if(!validateEmail(email)){
        return createError("Email inválido");
        }

        product_id = parseInt(product_id)

        if(isNaN(product_id)){
        return createError("Produto inválido");
        }
       
        if(password.length < 2){
        setLoading(false);
        return createError("Senha inválida");
        }
        var response;
        setLoading(true);
        try{
        response = await api("POST","/api/v1/user",{name,email:email.toLowerCase(),password,product_id})
        }
        catch(err){
            
            setLoading(false);
            return createError("Erro no server");
        }
        if(response.status == "error"){
        setLoading(false);
        return createError(response.error);
        }

        try{

          await AsyncStorage.multiSet([["@Eleven_userInfo",JSON.stringify(response.user)],["@Eleven_token",JSON.stringify(response.token)]])

        }
        catch(err){
         
          setLoading(false);
          return createError(err);
        }
        setUserInfo(response.user)
        setUserToken(response.token)
        setLoading(false);
        return createSuccess(response)
     


    }


    
return (
    <AuthContext.Provider value={{userInfoContext:userInfo,userToken:userToken,logIn,logOut,getInfoAsyncStorage,register,setLoading,getInfo,loading:Loading}}>
        {children}
    </AuthContext.Provider>

)


}
export default AuthContext;