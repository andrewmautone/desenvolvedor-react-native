 //retorna um erro
 function createError(error) {

     throw {
         status: "error",
         error: error
     }

 }

 //Checa se o objeto não é nulo, se for retorna erro
 function checkReturned(jsonResponse, error) {

     if (Object.keys(jsonResponse).length == 0)
         return createError(error)

     return {
         status: "success"
     }

 }



 function validateEmail(email) {
     const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     return reg.test(String(email).toLowerCase());
 }

 function validateUser(body) {

    const {email,password,name,product_id} = body
    
     if (!validateEmail(body.email))
         throw createError("Email inválido")

     if (!body.password) {
         throw createError("Senha inválida")
     }
     if (body.password.length < 5) {
         throw createError("Senha muito curta")
     }

     if (!body.name)
         throw createError("Nome inválido")
     if (body.name.length < 2)
         throw createError("Nome muito curto")

     if (!body.product_id)
         throw createError("Produto inválido")
        
    return {email:email,password:password,name:name,product_id:product_id}

 }
 module.exports = {
     createError,
     validateEmail,
     validateUser,
     checkReturned
 }