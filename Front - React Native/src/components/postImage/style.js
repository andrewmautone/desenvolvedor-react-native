import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
    

    imgContainer:{

        width:"100%",
        height:210,
        justifyContent:"center",
        alignItems:"center",
        borderRadius:10,
        backgroundColor:"#ddd",
        marginVertical:10,
        overflow:'hidden'
        
       
    
    },
    
    img:{
      
        margin:0,
        flex:1,
        resizeMode:"contain",
        borderRadius:10,
        overflow:'hidden'
      
        
       
    
    },


});

export default Styles;