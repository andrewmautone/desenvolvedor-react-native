import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, View, Text, ScrollView, StatusBar } from 'react-native';
import InputText from '../components/inputText';
import Button from '../components/button';
import AuthContext from '../context/auth';



const Login = (props) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMsg, setErrorMsg] = useState(false);
  const { logIn, getInfoAsyncStorage, userInfoContext } = useContext(AuthContext)
  useEffect(() => {

    getInfoAsyncStorage();

  }, [])
  useEffect(() => {

    if (userInfoContext)
      setEmail(userInfoContext.email)

  }, [userInfoContext])



  const login = async () => {
    setErrorMsg()
    const response = await logIn(email, password)

    if (response.status == "error") {
      setErrorMsg(response.error)

    }

  }





  return (

    <ScrollView style={{ width: '100%', height: '100%' }}>

      <View style={styles.container}>

        <View style={[styles.inputContainer, { margin: 45 }]}>

          <InputText otherProps={{ value: email }} autoCapitalize="none" textContentType="emailAddress" onChangeText={(text) => setEmail(text)} placeholder="Email" />
          <InputText textContentType="password" autoCapitalize="none" onChangeText={(text) => setPassword(text)} placeholder="Senha" password={true} />
          <Text style={{ color: "red" }}>{errorMsg}</Text>
        </View>
        <View style={styles.inputContainer}>

          <Button onPress={() => login({ email, password })} text="Entrar" backColor='#34bbe1' />
          <Button text="Esqueci a senha" style={{ marginBottom: 40, marginTop: 0 }} color='#34bbe1' backColor='rgba(0,0,0,0)' />
          <Button onPress={() => props.navigation.navigate('Register')} text="Cadastrar" />

        </View>

      </View>

    </ScrollView>


  );
}
const styles = StyleSheet.create({

  container: {

    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "space-between"


  },
  inputContainer: {
    width: '90%',
    alignItems: 'center'
  },
  Title: {
    fontSize: 40,

    margin: 60,




  }
});

export default Login;
