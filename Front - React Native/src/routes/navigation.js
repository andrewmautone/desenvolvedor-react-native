import React,{useContext} from 'react';
import Loading from '../components/loading'
import AuthContext from '../context/auth'
import Auth from '../routes/auth';
import App from '../routes/app';





const Navigation = () =>{
    
 
    
  const {loading,userToken} = useContext(AuthContext)

    
    return(
     
      !userToken?
      <>
      {loading &&<Loading/>}
      <Auth/>
      </>
      :
      <>
      {loading &&<Loading/>}
      <App/>
      </>
  
        
);
}


export default Navigation;