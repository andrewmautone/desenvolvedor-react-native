const datalake = require('../database/datalake')
const {createError} = require('../helper/helper')
class Product{

index = async (id)=>{

    if(!id)
    createError("Id inválido")
    
return await datalake.get("products/",{
        params: {
            id: id
          }
    
    }).then((product)=>{

        if(!product.data[0])
        createError("Produto inválido")

        return product.data[0]

    })

}
all = async ()=>{
  
return await datalake.get("products/").then((products)=>{
        
        
        
        if(!products.data[0])
        createError("Erro ao pegar produtos")

        return products.data

})

}



}

module.exports = new Product;