import React, { useEffect, useContext, useState } from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import Highlighter from 'react-native-highlight-words';
import PostImage from '../components/postImage';
import api from '../services/api';
import moment from 'moment';
import AuthContext from '../context/auth';

const Post = ({ navigation, route }) => {

    const { userInfoContext, setLoading, logOut } = useContext(AuthContext);
    const [postContent, setPost] = useState(null);

    useEffect(() => {
        async function getPost() {
            setLoading(true);
            var response = await api("GET", `/api/v1/user/${userInfoContext.id}/posts/${route.params.id}`)

            if (!response) {
                alert(response.error)
                setLoading(false);
                navigation.goBack()
            }
            if (response.status == "error" && response.error == "Token inválido") {
                Alert.alert(response.error)
                logOut();

            }

            setPost(response)
            setLoading(false);
        }
        getPost()


    }, [])

    if (postContent)
        return (

            <ScrollView style={styles.scrollView}>
                <View style={styles.container}>
                    <View style={styles.postContaier}>

                        <PostImage />
                        <View style={styles.textContainer}>
                            <Text style={styles.title}>{postContent.title}</Text>
                            <Text style={styles.author}>{postContent.author}</Text>
                            <Highlighter
                                style={styles.content}
                                searchWords={[!!route.params.searchWords ? route.params.searchWords : ""]}
                                highlightStyle={{ fontWeight: 'bold' }}
                                textToHighlight={postContent.content}
                                style={styles.content} />
                            <Text style={styles.content}>{moment(postContent.date).format("DD/MM/YYYY")}</Text>
                        </View>


                    </View>





                </View>

            </ScrollView>
        ); else {
        return (

            <ScrollView style={styles.scrollView}>
                <View style={styles.container}>
                    <View style={styles.postContaier}>

                        <PostImage />

                    </View>





                </View>

            </ScrollView>
        );



    }

}

const styles = StyleSheet.create({
    content: {
        marginVertical: 10

    },
    author: {
        marginTop: 0,
        marginLeft: 0,
        fontSize: 20,


    },
    textContainer: {
        width: '100%',
        marginTop: 10

    },
    title: {
        fontSize: 35,
        fontWeight: 'bold'


    },
    scrollView: {
        width: '100%',



    },
    container: {
        width: '100%',
        alignItems: 'center'


    },
    postContaier: {
        marginVertical: 20,
        width: '90%',
        alignItems: 'center'


    }



});
export default Post;
