import React, { useEffect, useState, useRef, useContext } from 'react';
import { Text, View, StyleSheet, ScrollView, Image } from 'react-native';
import InputText from '../components/inputText'
import Button from '../components/button'
import AsyncStorage from '@react-native-community/async-storage'
import api from '../services/api';
import AuthContext from '../context/auth';

const Profile = (props) => {
    
    const { logOut, userInfoContext,getInfo } = useContext(AuthContext)
    const [userInfo, setUserInfo] = useState({});
    const [product, setProduct] = useState('');
    const refScrollView = useRef(null);

    useEffect(() => {

        async function getInfoAsyncStorage() {
            
            if (!userInfoContext) {
                alert("Erro de autenticação")
                logOut();
            }

            if (getUserInfo.status == "error") {
                alert(getUserInfo.error)
                logOut();


            }
            getUserInfo = getInfo()

            var productName = (await api("GET", '/api/v1/products/' + getUserInfo.product_id)).name

            if (getUserInfo.status == "error") {

                logOut();
            }
            setProduct(productName)
            setUserInfo(getUserInfo)
        }
        getInfoAsyncStorage();



    }, [])
    return (
        <ScrollView ref={refScrollView} style={styles.scrollView}>
            <View style={styles.container}>

                <View style={styles.background}>
                    <View style={styles.pictureContainer}>
                        <Image style={styles.picture} source={require('../image/profilePic.jpg')} />
                    </View>

                </View>
                <View style={styles.bottomContainer}>

                    <Text style={styles.name}>{userInfo.name}</Text>

                    <InputText otherProps={{ value: userInfo.email, editable: false }} onFocus={() => setTimeout(() => refScrollView.current.scrollToEnd({ animated: true }), 500)} text={userInfo.email} placeholder="Email" />
                    <InputText otherProps={{ value: product, editable: false }} onFocus={() => setTimeout(() => refScrollView.current.scrollToEnd({ animated: true }), 500)} placeholder="Produto" />
                    <Button text="Fazer logout" onPress={async () => {
                        logOut()

                    }} backColor='#34bbe1' />

                </View>
            </View>
        </ScrollView>
    );
}

export default Profile;

const styles = StyleSheet.create({
    scrollView: {
        height: '100%',
        width: '100%'
    },
    container: {
        flex: 1,
        alignItems: 'center',
    },
    picture: {
        width: '95%',
        height: '95%',
        borderRadius: 100,
    },
    pictureContainer: {


        top: 5,
        width: 180,
        borderRadius: 90,
        aspectRatio: 1,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.05)',
        backgroundColor: "#eee",
        alignItems: "center",
        justifyContent: "center"

    },
    background: {
        alignItems: 'center',
        width: '100%',
        height: 140,
        backgroundColor: '#34bbe1'

    },
    bottomContainer: {

        alignItems: 'center',
        width: '90%',
        alignItems: 'center'
    },
    name: {
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 65
    }









})