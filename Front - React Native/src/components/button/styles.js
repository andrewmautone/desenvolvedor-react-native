import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
  

    TouchableOpacity:{

        width:"100%",
        height:55,
        borderRadius:50,
        backgroundColor:'#777',
        margin:15,
        alignItems:'center',
        justifyContent:'center'


    },
    text:{

        fontSize:18,
        color:"#fff"

    }

});

export default Styles;