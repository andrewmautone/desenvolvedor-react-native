const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser');

app.use(bodyParser.json());

require('./controllers/userController')(app)
require('./controllers/productsController')(app)
require('./controllers/postsController')(app)

app.listen(port, () => {
  console.log(`http://localhost:${port}`)
})