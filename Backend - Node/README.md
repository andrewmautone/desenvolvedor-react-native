# Teste Dev - React Native + Node

## BackEnd

Para iniciar a aplicação backend
```sh
$ npm i
```
```sh
$ node src/index.js
```

## EndPoints

- **GET** `/api/v1/products`
- **GET** `/api/v1/products/<id do produto>`
- **POST** `/api/v1/user/auth`
- **POST** `/api/v1/user/`
- **GET** `/api/v1/user/<id do usuário>`
- **PUT** `/api/v1/user/<id do usuário>`
- **GET** `/api/v1/user/<id do usuário>/posts`
- **GET** `/api/v1/user/<id do usuário>/posts/<id do post>`
