import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({

    container: {
        borderRadius: 10,
        width: "100%",
        height: 55,
        flexDirection: "row",
        borderWidth: 1,
        borderColor: '#ccc',
        backgroundColor: '#eee',
        margin: 15,
        alignContent: 'center',
        justifyContent: 'space-between'





    },
    textInput: {
        margin: 3,
        marginHorizontal: 15,
        fontSize: 17,
        flex: 1,



    },
    showButtom: {

        alignContent: 'center',
        justifyContent: 'center',
        padding: 15
    },
    showButtomText: {

        color: '#34bbe1',
        fontSize: 16

    }



});

export default Styles;