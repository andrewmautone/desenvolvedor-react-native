import React from 'react';
import { View, Modal ,ActivityIndicator } from 'react-native';
import styles from './styles'
const Loading = (props) => {
    
   return (
    <Modal transparent={true} style={styles.fullScreen}>
   
       <View style={styles.view}>
       <ActivityIndicator color='#34bbe1' size="large"/>

       </View>
      
      
    </Modal>
)};

export default Loading;
