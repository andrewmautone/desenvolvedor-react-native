import AsyncStorage from '@react-native-community/async-storage'
const baseUrl = 'http://localhost:3000'


const api = async(method, url, data) => {

    var fetchOptions = {
        method
    }

    if (data) {
        if (Object.keys(data).length > 0)
            fetchOptions.body = JSON.stringify(data)
    }
    const Authorization = JSON.parse((await AsyncStorage.getItem("@Eleven_token")))

    fetchOptions.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
        //Se estiver autenticado adiciona o header
    if (Authorization)
        fetchOptions.headers.authorization = "Bearer " + Authorization


    return await fetch(baseUrl + url,
        fetchOptions
    ).then(response => {

        return response.json()
    }).catch(error => {
        return {...error,
            status: "error"
        }
    })
}
export default api;