import React, { useState, useContext, useEffect } from 'react';
import { StyleSheet, FlatList, View, RefreshControl } from 'react-native';
import InputText from '../components/inputText';
import Post from '../components/posts';
import AuthContext from '../context/auth';
import api from '../services/api';






const Feed = (props) => {


  const [posts, setPosts] = useState({})
  const [refreshing, setRefreshing] = useState(true)
  const [searchBarText, setSearchBarText] = useState("")
  const { userInfoContext, logOut } = useContext(AuthContext)

  async function getPosts() {
    setRefreshing(true);
    var response = await api("GET", `/api/v1/user/${userInfoContext.id}/posts`)
    if (response.status)
      if (response.status == "error") {
        alert(response.error)
        logOut()
      }
    setRefreshing(false)
    setPosts(response)

  }
  useEffect(() => {






    getPosts();

  }, [])
  return (



    <View style={styles.container}>


      <View style={styles.inputContainer}>
        <InputText onChangeText={(text) => setSearchBarText(text)} placeholder="Buscar" style={{ borderRadius: 80 }} />
        <View>
          <FlatList

            keyExtractor={(item, index) => item.id.toString()}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={() => { getPosts() }} />
            }
            style={styles.listFooter}
            data={posts}
            renderItem={({ index, item }) => {
              if (searchBarText.length > 0) {
                if (item.content.includes(searchBarText))
                  return <Post id={item.id} content={item.content} onPress={() => props.navigation.navigate('Post', { id: item.id, searchWords: searchBarText })} boldWords={[searchBarText]} title={item.title} content={item.content} date={item.date} />


              } else {

                return <Post onPress={() => props.navigation.navigate('Post', { id: item.id, searchWords: searchBarText })} title={item.title} content={item.content} date={item.date} />

              }

            }
            }
          />
        </View>

      </View>


    </View>


  );
}
const styles = StyleSheet.create({
  listFooter: {
    marginBottom: 100,

  },
  container: {


    flex: 1,
    alignItems: "center",
    justifyContent: "space-between"


  },
  inputContainer: {
    width: '90%',
    alignItems: 'center',

  },
  Title: {
    fontSize: 40,

    margin: 60,




  }
});
export default Feed;
