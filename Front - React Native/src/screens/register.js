import React, { useState, useContext, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import InputText from '../components/inputText';
import CPicker from '../components/picker';
import api from '../services/api';
import Button from '../components/button';
import AuthContext from '../context/auth'



const Register = (props) => {

  const { register, setLoading } = useContext(AuthContext)

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [product, setProduct] = useState(0);
  const [password, setPassword] = useState("");
  const [errorMsg, setErrorMsg] = useState("");
  const [dataProducts, setDataProducts] = useState(null);
  async function createUser(name, email, password, product) {

    setErrorMsg("")
    var response = await register(name, email, password, product)

    if (response.status == "error")
      setErrorMsg(response.error)

  }
  useEffect(() => {


    async function getProducts() {
      setLoading(true)
      var response = await api("GET", "/api/v1/products")
      if (response.status == "error") {

        setLoading(false)
        alert("Erro no servidor")
        props.navigation.goBack()
        return;
      }

      setDataProducts(response)
      setLoading(false)

    }
    getProducts()


  }, [])
  return (

    <ScrollView style={{ width: '100%', height: '100%' }}>
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <InputText onChangeText={text => setName(text)} autoCapitalize="words" placeholder="Nome" />
          <InputText onChangeText={text => setEmail(text)} autoCapitalize="none" textContentType="emailAddress" placeholder="Email" />
          <CPicker selectedValue={(value) => setProduct(value)} data={dataProducts} />
          <InputText onChangeText={text => setPassword(text)} placeholder="Senha" password={true} />
          <Text style={{ color: "red" }}>{errorMsg}</Text>
          <Button onPress={async () => await createUser(name, email, password, product)} text="Registrar-se" backColor='#34bbe1' />
        </View>
      </View>

    </ScrollView>
  );
}
const styles = StyleSheet.create({

  container: {


    flex: 1,
    alignItems: "center",
    justifyContent: "space-between"


  },
  inputContainer: {
    width: '90%',
    alignItems: 'center'
  },
  Title: {
    fontSize: 40,

    margin: 60,




  }
});
export default Register;
