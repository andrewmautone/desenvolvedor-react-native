import React from 'react';
import {View,Image } from 'react-native';
import Styles from './style';
const PostImage = () => {
    
  
    
    return(
    <View style={Styles.imgContainer}>
        <Image resizeMode="contain" resizeMethod="resize" style={Styles.img} source={require('../../image/postImage.jpg')}/>
    </View>
)};

export default PostImage;
