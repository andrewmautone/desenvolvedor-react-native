const express = require('express')
const router = express.Router();
const userModel = require('../models/userModel')
const postModel = require('../models/postModel')

const authMiddleware = require("../middleware/auth")


router.get('/:userId/posts', authMiddleware, async(req, res) => {

    //Verifica se o id do usuario logado é o mesmo que a da request
    if(req.userId != req.params.userId){
        return res.status(403).send({status:"erro",erro:"Usuario não condiz com o token"})
    }
    let user;
    try {
        user = await userModel.index(req.params.userId);
    } catch (err) {
        res.status(404).send("Usuario não existe");
    }

    //Retorna posts ou erro
    try {
        const response = await postModel.all(user.product_id);
        return res.status(201).send(response)
    } catch (err) {

        return res.status(404).send(err)

    }

})
router.get('/:userId/posts/:postId', authMiddleware, async(req, res) => {

    
        if(req.userId != req.params.userId){

          return res.status(403).send({status:"erro",erro:"Usuario não condiz com o token"})

        }
        //Verifica se o usuario existe
        let user;
        try {
            user = await userModel.index(req.params.userId);
        } catch (err) {
            res.status(404).send("Usuario não existe");
        }

        let post;
        try {

            post = await postModel.index(req.params.postId);

        } catch (err) {

            return res.status(404).send(err)

        }

        if (post.products.includes(user.product_id))
            return res.status(200).send(post)

        return res.status(403).send({status:"erro",erro:"Voce não tem acesso a esse post"})


    })

module.exports = app => app.use('/api/v1/user', router)