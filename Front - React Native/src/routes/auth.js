import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/login';
import Register from '../screens/register';

const Auth = () => {

    const Stack = createStackNavigator();

return(
    <>
       
    <Stack.Navigator  
    screenOptions={{
     
    
    
     headerStyle:{backgroundColor:'transparent',elevation:0,shadowOpacity: 0,height:100},
     headerTitleAlign:"center",
     headerTitleStyle:{fontSize:40}
     }}>
       
       <Stack.Screen name="Login" component={Login}  options={{
          headerTitle: 'Acessar',
          headerStyle:{backgroundColor:'transparent',elevation:0,shadowOpacity: 0,height:100},
          headerTitleAlign:"center",
          headerTitleStyle:{fontSize:40}
          
          }} />
       <Stack.Screen name="Register" component={Register}  options={{ title: 'Registrar' }} />
      
       
     
     </Stack.Navigator>
     </>  
);
 }

export default Auth;


