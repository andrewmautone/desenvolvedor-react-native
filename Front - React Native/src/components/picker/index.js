import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import Styles from './styles'
import RNPickerSelect from 'react-native-picker-select';
const CPicker = (props) => {


    const [selectedLabel, setSelectedLabel] = useState("Produtos")
    const [data, setData] = useState(null)

    useEffect(() => {

        let dataMap;
        try {
            if (!!props.data.map) {
                dataMap = props.data.map((item) => {
                    return { label: item.name, value: item.id, key: item.id }


                });
            }
            setData(dataMap)

        }
        catch (err) {


        }




    }, [props.data])


    return (
        data ?
            <View style={Styles.container}>
                <RNPickerSelect
                    doneText="Selecionar"
                    style={
                        {
                            inputIOSContainer: { ...Styles.container, alignSelf: "center" },
                            inputIOS: { ...Styles.textInput },
                            viewContainer: { width: "100%", height: "100%", marginHorizontal: 10, justifyContent: "center" },
                            inputAndroid: { color: "#000" },
                        }
                    }
                    placeholder={{ label: "Produtos", value: null }}
                    onValueChange={(value) => props.selectedValue(value)}
                    items={data}

                />

            </View>

            :

            <View />
    )
}
export default CPicker;
