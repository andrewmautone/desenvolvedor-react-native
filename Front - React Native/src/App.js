/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { StatusBar } from 'react-native';
import React from 'react';
import Navigation from './routes/navigation'
import { AuthProvider } from './context/auth'



const Theme = {

  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#fff',
  },
};



const App = () => {

  return (
    <AuthProvider>
      <NavigationContainer theme={Theme}>

        <StatusBar backgroundColor="#efefef" barStyle='dark-content' />
        <Navigation />

      </NavigationContainer>
    </AuthProvider>

  )
};


export default App;
