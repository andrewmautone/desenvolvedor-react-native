const blog = require('../database/blog')
const {createError} = require('../helper/helper')
class Product{

index = async (postId)=>{

    return await blog.get("posts/",{
        params: {
            id: postId,
            _sort:"date",
            _order:"desc"
          }
    
    }).then((post)=>{
    
        
            if(!post.data[0])
            createError("Id inválido")
            
            
           post.data[0].content =  post.data[0].content.replace(/<(.|\n)*?>/g, '');
            return post.data[0]
    
    })
    
   

}
all = async (products_id)=>{

return await blog.get("posts/",{
    params: {
        products_like: products_id,
        _sort:"date",
        _order:"desc"
      }

}).then((posts)=>{

    
    posts.data.forEach(Post => {
        Post.content = Post.content.replace(/<(.|\n)*?>/g, '');
    })
        return posts.data

})

}



}

module.exports = new Product;