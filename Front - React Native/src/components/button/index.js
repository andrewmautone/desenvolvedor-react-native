import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Styles from './styles'

const Button = (props) => (
    <TouchableOpacity style={[Styles.TouchableOpacity,!!props.backColor&&{backgroundColor:props.backColor},{...props.style}]} onPress={props.onPress} >
        <Text allowFontScaling={true} style={[Styles.text,!!props.color&& {color:props.color}]} >{props.text}</Text>
    </TouchableOpacity>
);

export default Button;
