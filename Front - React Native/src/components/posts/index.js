import React from 'react';
import { Text, View,TouchableOpacity } from 'react-native';
import moment from 'moment';
import Styles from './styles'
import PostImage from '../postImage'
import Highlighter from 'react-native-highlight-words';
function TrimString(string){

 return string.slice(0,250) + " ..."

}
function dateFromNow(date){

   
    moment.updateLocale('pt-br', 
    {relativeTime : {
        future : 'em %s',
        past : '%s atrás',
        s : 'segundos',
        m : 'um minuto',
        mm : '%d minutos',
        h : 'uma hora',
        hh : '%d horas',
        d : 'um dia',
        dd : '%d dias',
        M : 'um mês',
        MM : '%d meses',
        y : 'um ano',
        yy : '%d anos'}
    },)

return moment(date).fromNow();

}

const Post = (props) => {

var string = props.content?props.content:""

return(
<TouchableOpacity onPress={props.onPress?props.onPress:()=>{}} style={[Styles.container,props.style]}>
        <PostImage/>
       <View style={Styles.textContainer}>
        <Text style={Styles.header}>{props.title?props.title:"Header"}</Text>
        <Highlighter
        style={Styles.content}
        highlightStyle={{fontWeight: 'bold'}}
        searchWords={props.boldWords?props.boldWords:[]}
        textToHighlight={TrimString(string)}
        />
        <Text style={Styles.data}>{dateFromNow(props.date)}</Text>

       </View>

</TouchableOpacity>
);


}
    

export default Post;
