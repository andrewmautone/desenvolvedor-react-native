# Teste Dev - React Native + Node

## FrontEnd
```sh
$ npm i
```

Caso use ios, usar o ```pod instal``` na pasta IOS


```sh
$ react-native run-android/ios
```

Se for necessário trocar o ip de acesso ao backend, trocar **baseUrl** no arquivo:
```sh
src/services/api.js
```
