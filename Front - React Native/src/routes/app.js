import React from 'react';
import { View, StatusBar, Platform } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import Profile from '../screens/profile'
import Feed from '../screens/feed'
import Post from '../screens/uniquePost'


const Tab = createBottomTabNavigator();
function getHeader(route) {

  const routeName = getFocusedRouteNameFromRoute(route);
  const ProfileStyle = { headerTitle: 'Perfil', headerTitleStyle: { color: '#fff', fontSize: 40 }, headerStyle: { backgroundColor: '#34bbe1', elevation: 0, shadowOpacity: 0, height: 100 } };
  switch (routeName) {
    case 'Feed':
      Platform.OS == "android" ? StatusBar.setBackgroundColor('#fff') : null
      return { headerTitle: 'Publicações', headerTitleStyle: { color: '#000', fontSize: 40 }, headerStyle: { backgroundColor: 'transparent', elevation: 0, shadowOpacity: 0, height: 100 } };
    case 'Profile':
      Platform.OS == "android" ? StatusBar.setBackgroundColor('#34bbe1') : null

      return ProfileStyle
    default:
      Platform.OS == "android" ? StatusBar.setBackgroundColor('#34bbe1') : null
      return ProfileStyle


  }
}
function TabNavigator({ navigation, route }) {

  React.useLayoutEffect(() => {
    navigation.setOptions(getHeader(route));
  }, [navigation, route]);

  return (
    <>
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: '#34bbe1',
          inactiveTintColor: '#555',
        }}
        screenOptions={{
          headerStyle: { backgroundColor: 'transparent', elevation: 0, shadowOpacity: 0, height: 100 },
          headerTitleAlign: "center",
          headerTitleStyle: { fontSize: 40 },
          headerMode: 'screen',
          tabBarIcon: ({ focused, color, size }) => {
            let iconConfig;

            if (route.name === 'Profile') {
              iconConfig = focused
                ? { backgroundColor: '#34bbe1', borderWidth: 0 }
                : { backgroundColor: '#ddd' };
            } else if (route.name === 'Feed') {
              iconConfig = focused ? { backgroundColor: '#34bbe1', borderWidth: 0 } : { backgroundColor: '#ddd' };
            }

            // You can return any component that you like here!
            return <View style={[{ width: 25, height: 25, borderRadius: 12.5, borderWidth: 0.1 }, iconConfig]} />;
          },
        }}>

        <Tab.Screen name="Profile" component={Profile} options={{
          title: 'Perfil',
          headerStyle: {
            backgroundColor: '#34bbe1',
            shadowOpacity: 0, elevation: 0
          },
          headerTitleStyle: { color: '#fff', fontSize: 40 },


        }} />

        <Tab.Screen name="Feed" component={Feed} options={{
          title: 'Publicações',
          headerStyle: {
            backgroundColor: '#34bbe1',
            shadowOpacity: 0, elevation: 0
          },
          headerTitleStyle: { color: '#fff', fontSize: 40 },


        }} />



      </Tab.Navigator>
    </>
  );


}

const App = () => {

  const Stack = createStackNavigator();

  return (
    <>

      <Stack.Navigator
        screenOptions={{
          headerStyle: { backgroundColor: 'transparent', elevation: 0, shadowOpacity: 0, height: 100 },
          headerTitleAlign: "center",
          headerTitleStyle: { fontSize: 40 },
          headerMode: 'screen',
        }}>

        <Stack.Screen name="Profile" component={TabNavigator}
          options={{

            headerStyle: {
              backgroundColor: '#34bbe1',
              shadowOpacity: 0, elevation: 0
            },
            headerTitleStyle: { color: '#fff', fontSize: 40 },



          }} />
        <Stack.Screen name="Post" component={Post}
          options={{

            headerStyle: {
              backgroundColor: 'transparent',
              shadowOpacity: 0, elevation: 0
            },
            headerTitleStyle: { color: '#fff', fontSize: 0 },



          }} />



      </Stack.Navigator>
    </>
  );
}

export default App;