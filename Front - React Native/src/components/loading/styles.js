import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
    
    fullScreen:{
        position:'absolute',
        top:0,
        bottom:0,
        right:0,
        left:0,
       
        alignItems:"center",
        paddingTop:"100%",
       
       


    },
    view:{

        flex:1,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"rgba(0,0,0,0.8)",
    }



});

export default Styles;