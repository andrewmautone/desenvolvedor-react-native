const express = require('express')
const router = express.Router();
const userModel = require("../models/userModel");
const productModel = require("../models/productModel");
const authMiddleware = require("../middleware/auth")


//Retorna informaçoes do user
router.get('/:userId', authMiddleware, async(req, res) => {

        try {
            const response = await userModel.index(req.params.userId)
            return res.status(200).send(response)
        } catch (err) {
            return res.status(404).send(err)
        }




    })
    //Altera informações do usuario
router.put('/:userId', authMiddleware, async(req, res) => {

        //Verifica se o id do usuario logado é o mesmo que a da request
        if (req.userId != req.params.userId) {
            return res.status(403).send({
                status: "error",
                erro: "Usuario não condiz com o token"
            })
        }
        const body = req.body

        //verifica se o produto queee vai ser alterado existe 
        try {
            await productModel.index(body.product_id)
        } catch (err) {
            return res.status(404).send(response)
        }

        //Atualiza usuario
        try {
            const response = await userModel.update(req.params.userId, body)
            return res.status(200).send(response)

        } catch (err) {
            return res.status(404).send({
                err
            })
        }



    })
    //Adiciona usuarios no sistema
router.post('/', async(req, res) => {



    const body = req.body

    //Verifica se o email já está em uso
    try {
        await userModel.verifyEmail(body.email)
    } catch (err) {
        return res.status(400).send(err)
    }

    //Verifica se o produto existe
    try {
        await productModel.index(body.product_id)
    } catch (err) {
        return res.status(404).send(err)
    }

    //Adiciona usuario ao sistema
    try {
        const response = await userModel.insert(req.body)
        return res.status(200).send(response)
    } catch (err) {
        return res.status(404).send(err)
    }




})

router.post('/auth', async(req, res) => {
    try {
        const response = await userModel.login(req.body)
        return res.status(200).send(response)
    } catch (err) {
        return res.status(404).send(err)
    }

})



module.exports = app => app.use('/api/v1/user', router)