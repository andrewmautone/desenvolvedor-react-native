import React,{useState} from 'react';
import { Text, View,TextInput,TouchableOpacity } from 'react-native';

import Styles from './styles'

const InputText = (props) => {

  const [showPass,setShowPass] = useState(props.password)
  const [value,setValue] = useState("")
return(

<View style={[Styles.container,props.style]}>
        <TextInput 
        setText={(text)=>setValue(text)}
        {...props.otherProps}
        onFocus={props.onFocus?(e)=>props.onFocus(e):()=>{}}
        autoCapitalize={props.autoCapitalize} 
        textContentType={props.textContentType} 
        selectTextOnFocus={true} 
        onChangeText={props.onChangeText?(e)=>{props.onChangeText(e);setValue(e)}:()=>{}} 
        style={Styles.textInput} 
        allowFontScaling={true}
        secureTextEntry={showPass}
        placeholder={props.placeholder}/>

        {props.password && 
        <TouchableOpacity onPress={() => setShowPass(!showPass)} style={Styles.showButtom}>
            <Text style={Styles.showButtomText}>{showPass?"Mostar":"Esconder"}</Text>
        </TouchableOpacity>}
    </View>
);


}
    

export default InputText;
